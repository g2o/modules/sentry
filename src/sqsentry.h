#pragma once

#include <sqapi.h>
#include <sentry.h>
#include <unordered_map>

class SqSentry
{
public:
	static void bind(HSQUIRRELVM vm);

	static void init(Sqrat::Table arg);

	static SQRESULT setTag(HSQUIRRELVM vm);
	static SQRESULT setContext(HSQUIRRELVM vm);

	static void captureEvent(Sqrat::Table arg);

private:
	static std::unordered_map<Sqrat::string, Sqrat::string> tags_;
	static std::unordered_map<Sqrat::string, sentry_value_t> contexts_;
};