#include <sqapi.h>
#include "sqsentry.h"

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);

	SqSentry::bind(vm);

	return SQ_OK;
}
