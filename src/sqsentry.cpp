#include "sqsentry.h"

template<typename T>
static Sqrat::SharedPtr<T> sqrat_table_getvalue(Sqrat::Table& table, const char* key)
{
	auto value = table.HasKey(key) ? table.GetValue<T>(key) : nullptr;
	return value;
}

static void sentry_set_sqrat_table_int(Sqrat::Table& table, const char* key, sentry_value_t& sentry_object, const char* sentry_key = nullptr)
{
	if (sentry_key == nullptr)
		sentry_key = key;

	auto package = sqrat_table_getvalue<SQInteger>(table, key);
	if (package != nullptr)
		sentry_value_set_by_key(sentry_object, key, sentry_value_new_int32(*package));
}

static void sentry_set_sqrat_table_string(Sqrat::Table& table, const char* key, sentry_value_t& sentry_object, const char* sentry_key = nullptr)
{
	if (sentry_key == nullptr)
		sentry_key = key;

	auto package = sqrat_table_getvalue<Sqrat::string>(table, key);
	if (package != nullptr)
		sentry_value_set_by_key(sentry_object, key, sentry_value_new_string(package->c_str()));
}

std::unordered_map<Sqrat::string, Sqrat::string> SqSentry::tags_;
std::unordered_map<Sqrat::string, sentry_value_t> SqSentry::contexts_;

void SqSentry::bind(HSQUIRRELVM vm)
{
	Sqrat::ConstTable()
		/* squirreldoc (const)
		*
		* Represents debug event level.
		*
		* @category SentryLevel
		* @side		shared
		* @name		SENTRY_LEVEL_DEBUG
		*
		*/
		.Const("SENTRY_LEVEL_DEBUG", SENTRY_LEVEL_DEBUG)
		/* squirreldoc (const)
		*
		* Represents debug info level.
		*
		* @category SentryLevel
		* @side		shared
		* @name		SENTRY_LEVEL_INFO
		*
		*/
		.Const("SENTRY_LEVEL_INFO", SENTRY_LEVEL_INFO)
		/* squirreldoc (const)
		*
		* Represents debug warning level.
		*
		* @category SentryLevel
		* @side		shared
		* @name		SENTRY_LEVEL_WARNING
		*
		*/
		.Const("SENTRY_LEVEL_WARNING", SENTRY_LEVEL_WARNING)
		/* squirreldoc (const)
		*
		* Represents debug error level.
		*
		* @category SentryLevel
		* @side		shared
		* @name		SENTRY_LEVEL_ERROR
		*
		*/
		.Const("SENTRY_LEVEL_ERROR", SENTRY_LEVEL_ERROR)
		/* squirreldoc (const)
		*
		* Represents debug fatal level.
		*
		* @category SentryLevel
		* @side		shared
		* @name		SENTRY_LEVEL_FATAL
		*
		*/
		.Const("SENTRY_LEVEL_FATAL", SENTRY_LEVEL_FATAL)
	;

	Sqrat::RootTable()
		/* squirreldoc (class)
		*
		* This class represents the sentry interface.
		*
		* @static
		* @side		shared
		* @name		Sentry
		*
		*/
		.Bind("Sentry", Sqrat::Class<SqSentry>(vm, "Sentry")
			/* squirreldoc (method)
			*
			* This method initializes sentry instance, it must be called once during scripts startup.  
			*
			* @name		init
			* @param    (table) arg the init options:<br/>
			* - ``(string) arg.dsn`` the dsn url unique for a specific sentry project used for communication<br/>
			* - ``(string) arg.release=""`` release name of your project, e.g: g2o_server&#64;1.0.0<br/>
			* - ``(bool) arg.debug=false`` debug mode that toggles debug prints
			*
			*/
			.StaticFunc("init", SqSentry::init)
			/* squirreldoc (method)
			*
			* This method creates a global tag that will be attached to every captured event.  
			* Tags can be used for better grouping captured events.
			*
			* @name		setTag
			* @param    (string) name the tag name.
			* @param    (string|null) value the tag value, pass `null` to remove the global tag with specific name.
			*
			*/
			.SquirrelFunc("setTag", SqSentry::setTag, 3, ".ss")
			/* squirreldoc (method)
			*
			* This method sets a global context that will be attached to every captured event.  
			* Context can contain additional information about the occured event.  
			* Sentry also offers [**special contexts**](https://develop.sentry.dev/sdk/event-payloads/contexts/) that are rendered uniquely.
			*
			* @name		setContext
			* @param    (string) name the tag name.
			* @param    (string|null) value the table containing `string` keys and `int|string` values, pass `null` to remove the global context with specific name.
			*
			*/
			.SquirrelFunc("setContext", SqSentry::setContext, 3, ".s.")
			/* squirreldoc (method)
			*
			* This method sends a captured event with all of the provided additional metadata by the global/local scope to the sentry service.
			*
			* @name		captureEvent
			* @param    (table) arg the capture options:<br/>
			* - ``(string) arg.message`` the custom message that will be displayed in sentry UI<br/>
			* - ``(int) arg.level=SENTRY_LEVEL_ERROR`` event level. For more information see [Sentry Level](../../../shared-constants/sentrylevel/).<br/>
			* - ``(string) arg.logger=""`` message logger displayed in sentry UI<br/>
			* - ``(table) arg.tags=null`` table containing `string` keys and `string` values with local tags that will be only used for this event.<br/>
			* - ``(table) arg.contexts=null`` table containing `string` keys and `table` values (with `string` keys and `string|int` values) with local contexts that will be only used for this event.<br/>
			* - ``(array) arg.stacktrace=null`` array containing the table callstacks in specificied format (you can omit any value): `{package = string, filename = string, funcname = string, lineno = integer, colno = integer}`
			*
			*/
			.StaticFunc("captureEvent", SqSentry::captureEvent)
		)
	;
}

void SqSentry::init(Sqrat::Table arg)
{
	if (!arg.HasKey("dsn"))
		return Sqrat::Error::Throw(SqModule::vm, "arg.dsn required!");

	sentry_options_t* options = sentry_options_new();

	auto dsn = arg.GetValue<Sqrat::string>("dsn");
	sentry_options_set_dsn(options, dsn->c_str());

	if (arg.HasKey("release"))
	{
		auto release = arg.GetValue<Sqrat::string>("release");
		sentry_options_set_release(options, release->c_str());
	}

	if (arg.HasKey("debug"))
	{
		auto debug = arg.GetValue<bool>("debug");
		sentry_options_set_debug(options, *debug);
	}

	sentry_init(options);
}

SQRESULT SqSentry::setTag(HSQUIRRELVM vm)
{
	const SQChar* name;
	sq_getstring(vm, 2, &name);

	HSQOBJECT rawvalue;
	sq_getstackobj(vm, 3, &rawvalue);

	if (rawvalue._type == OT_NULL)
	{
		sentry_remove_tag(name);
		return SQ_OK;
	}

	if (sq_tostring(vm, 3) == SQ_ERROR)
		return SQ_ERROR;

	const SQChar* value;
	sq_getstring(vm, 4, &value);

	sentry_set_tag(name, value);
	tags_[name] = value;

	return SQ_OK;
}

SQRESULT SqSentry::setContext(HSQUIRRELVM vm)
{
	const SQChar* name;
	sq_getstring(vm, 2, &name);

	HSQOBJECT value;
	sq_getstackobj(vm, 3, &value);

	if (value._type == OT_NULL)
	{
		sentry_remove_context(name);
		return SQ_OK;
	}

	if (value._type != OT_TABLE)
		return sq_throwerror(vm, Sqrat::FormatTypeError(vm, 3, "table|null").c_str());

	Sqrat::Table sq_context = value;
	sentry_value_t context = sentry_value_new_object();

	Sqrat::Object::iterator it_context;
	while (sq_context.Next(it_context))
	{
		HSQOBJECT key = it_context.getKey();
		HSQOBJECT value = it_context.getValue();

		if (key._type != OT_STRING)
			continue;

		switch (value._type)
		{
			case OT_INTEGER:
				sentry_value_set_by_key(context, sq_objtostring(&key), sentry_value_new_int32(sq_objtointeger(&value)));
				break;

			case OT_STRING:
				sentry_value_set_by_key(context, sq_objtostring(&key), sentry_value_new_string(sq_objtostring(&value)));
				break;
		}
	}

	sentry_set_context(name, context);
	sentry_value_incref(context);

	if (contexts_.find(name) != contexts_.end())
		sentry_value_decref(contexts_[name]);

	contexts_[name] = context;

	return SQ_OK;
}

void SqSentry::captureEvent(Sqrat::Table arg)
{
	if (!arg.HasKey("message"))
		return Sqrat::Error::Throw(SqModule::vm, "arg.message required!");

	auto message = arg.GetValue<Sqrat::string>("message");

	sentry_level_e level = SENTRY_LEVEL_ERROR;
	if (arg.HasKey("level"))
		level = sentry_level_e(*arg.GetValue<int>("level"));

	Sqrat::SharedPtr<Sqrat::string> logger = nullptr;
	if (arg.HasKey("logger"))
		logger = arg.GetValue<Sqrat::string>("logger");

	sentry_value_t event = sentry_value_new_message_event(level, logger != nullptr ? logger->c_str() : nullptr, message->c_str());

	// hack for setting platform to javascript (cpp stacktrace can't be used for squirrel, so we have to replace it with javascript)
	sentry_value_remove_by_key(event, "platform");
	sentry_value_set_by_key(event, "platform", sentry_value_new_string("javascript"));

	if (arg.HasKey("tags"))
	{
		auto sq_tags = arg.GetValue<Sqrat::Table>("tags");
		if (sq_tags == nullptr)
			return Sqrat::Error::Throw(SqModule::vm, "arg.tags must be a table!");

		Sqrat::Object::iterator it;
		while (sq_tags->Next(it))
		{
			HSQOBJECT key = it.getKey();
			HSQOBJECT value = it.getValue();

			if (key._type != OT_STRING)
				continue;

			sentry_set_tag(sq_objtostring(&key), sq_objtostring(&value));
		}
	}

	if (arg.HasKey("contexts"))
	{
		auto sq_contexts = arg.GetValue<Sqrat::Table>("contexts");
		if (sq_contexts == nullptr)
			return Sqrat::Error::Throw(SqModule::vm, "arg.contexts must be a table!");

		Sqrat::Object::iterator it_contexts;
		while (sq_contexts->Next(it_contexts))
		{
			HSQOBJECT contexts_value = it_contexts.getValue();
			if (contexts_value._type != SQObjectType::OT_TABLE)
				continue;

			Sqrat::Table sq_context = contexts_value;
			sentry_value_t context = sentry_value_new_object();

			Sqrat::Object::iterator it_context;
			while (sq_context.Next(it_context))
			{
				HSQOBJECT key = it_context.getKey();
				HSQOBJECT value = it_context.getValue();

				if (key._type != OT_STRING)
					continue;

				switch (value._type)
				{
					case OT_INTEGER:
						sentry_value_set_by_key(context, sq_objtostring(&key), sentry_value_new_int32(sq_objtointeger(&value)));
						break;

					case OT_STRING:
						sentry_value_set_by_key(context, sq_objtostring(&key), sentry_value_new_string(sq_objtostring(&value)));
						break;
				}
			}

			sentry_set_context(it_contexts.getName(), context);
		}
	}

	if (arg.HasKey("stacktrace"))
	{
		auto sq_stacktrace = arg.GetValue<Sqrat::Array>("stacktrace");
		if (sq_stacktrace == nullptr)
			return Sqrat::Error::Throw(SqModule::vm, "arg.stacktrace must be an array!");

		sentry_value_t frames = sentry_value_new_list();

		Sqrat::Object::iterator it;
		while (sq_stacktrace->Next(it))
		{
			HSQOBJECT value = it.getValue();
			if (value._type != SQObjectType::OT_TABLE)
				continue;

			sentry_value_t frame = sentry_value_new_object();
			Sqrat::Table sq_frame = value;

			sentry_set_sqrat_table_string(sq_frame, "package", frame);
			sentry_set_sqrat_table_string(sq_frame, "filename", frame);
			sentry_set_sqrat_table_string(sq_frame, "funcname", frame, "function");
			sentry_set_sqrat_table_int(sq_frame, "lineno", frame);
			sentry_set_sqrat_table_int(sq_frame, "colno", frame);

			sentry_value_append(frames, frame);
		}

		sentry_value_t stacktrace = sentry_value_new_object();
		sentry_value_set_by_key(stacktrace, "frames", frames);

		sentry_value_set_by_key(event, "stacktrace", stacktrace);
	}

	// sending event
	sentry_capture_event(event);

	// cleanup
	if (arg.HasKey("tags"))
	{
		auto sq_tags = arg.GetValue<Sqrat::Table>("tags");

		Sqrat::Object::iterator it;
		while (sq_tags->Next(it))
		{
			HSQOBJECT key = it.getKey();
			HSQOBJECT value = it.getValue();

			if (key._type != OT_STRING)
				continue;

			if (value._type != OT_STRING)
				continue;

			sentry_remove_tag(sq_objtostring(&key));
		}

		for (auto pair : tags_)
			sentry_set_tag(pair.first.c_str(), pair.second.c_str());
	}

	if (arg.HasKey("contexts"))
	{
		auto sq_contexts = arg.GetValue<Sqrat::Table>("contexts");

		Sqrat::Object::iterator it_contexts;
		while (sq_contexts->Next(it_contexts))
		{
			HSQOBJECT contexts_value = it_contexts.getValue();
			if (contexts_value._type != SQObjectType::OT_TABLE)
				continue;

			sentry_remove_context(it_contexts.getName());
		}

		for (auto pair : contexts_)
			sentry_set_context(pair.first.c_str(), pair.second);
	}
}
