To integrate the sentry module with squirrel errors you need to intercept the error and send it to sentry.
Below you can find a very simple example of such integration script, it requires [BPackets](https://gitlab.com/bcore1/bpackets) and [Remote Error Tacker](https://gitlab.com/g2o/scripts/remoteerrortracker) scripts to be also included.

You can just copy the code, and name paste it to a file called `sentry.nut`.
After that, the only thing to do is to edit a specific strings, and paste your own values.

You can get the DSN and app_name directly from [sentry.io](https://sentry.io/),
you just need to create a new C application, sentry will give you everything that you'll need.

```js
addEventHandler("onInit", function()
{
	Sentry.init({
		dsn = "YOUR_DSN_URL",
		release = "YOUR_APP_NAME@MAJOR.MINOR.BUILD"
	})
})

local occured_errors = {}

addEventHandler("onScriptError", function(error, stackInfos)
{
	if (error in occured_errors)
		return

	local stacktrace = []
	local locals = {}

	foreach (level, stackInfo in stackInfos)
	{
		stacktrace.push({filename = stackInfo.src, funcname = stackInfo.func, lineno = stackInfo.line})

		foreach (localName, localValue in stackInfo.locals)
			locals[(level + 1) + ":" + localName] <- localValue != null ? localValue.tostring() : "null"
	}

	Sentry.captureEvent({
		message = error,
		level = SENTRY_LEVEL_ERROR,
		stacktrace = stacktrace
		contexts = {
			browser = {
				name = "g2o",
				version = "0.3.0"
			}

			locals = locals
		}
	})

	occured_errors[error] <- true
})
```