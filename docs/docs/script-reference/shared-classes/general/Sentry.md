---
title: 'Sentry'
---
# `static class` Sentry <font size="4">(shared-side)</font>

This class represents the sentry interface.


## Properties
No properties.

----

## Methods
### init

This method initializes sentry instance, it must be called once during scripts startup.

```cpp
void init(table arg)
```

**Parameters:**

* `table` **arg**: the init options:<br/> - ``(string) arg.dsn`` the dsn url unique for a specific sentry project used for communication<br/> - ``(string) arg.release=""`` release name of your project, e.g: g2o_server&#64;1.0.0<br/> - ``(bool) arg.debug=false`` debug mode that toggles debug prints
  

----
### setTag

This method creates a global tag that will be attached to every captured event.  
Tags can be used for better grouping captured events.

```cpp
void setTag(string name, string|null value)
```

**Parameters:**

* `string` **name**: the tag name.
* `string|null` **value**: the tag value, pass `null` to remove the global tag with specific name.
  

----
### setContext

This method sets a global context that will be attached to every captured event.  
Context can contain additional information about the occured event.  
Sentry also offers [**special contexts**](https://develop.sentry.dev/sdk/event-payloads/contexts/) that are rendered uniquely.

```cpp
void setContext(string name, string|null value)
```

**Parameters:**

* `string` **name**: the tag name.
* `string|null` **value**: the table containing `string` keys and `int|string` values, pass `null` to remove the global context with specific name.
  

----
### captureEvent

This method sends a captured event with all of the provided additional metadata by the global/local scope to the sentry service.

```cpp
void captureEvent(table arg)
```

**Parameters:**

* `table` **arg**: the capture options:<br/> - ``(string) arg.message`` the custom message that will be displayed in sentry UI<br/> - ``(int) arg.level=SENTRY_LEVEL_ERROR`` event level. For more information see [Sentry Level](../../../shared-constants/sentrylevel/).<br/> - ``(string) arg.logger=""`` message logger displayed in sentry UI<br/> - ``(table) arg.tags=null`` table containing `string` keys and `string` values with local tags that will be only used for this event.<br/> - ``(table) arg.contexts=null`` table containing `string` keys and `table` values (with `string` keys and `string|int` values) with local contexts that will be only used for this event.<br/> - ``(array) arg.stacktrace=null`` array containing the table callstacks in specificied format (you can omit any value): `{package = string, filename = string, funcname = string, lineno = integer, colno = integer}`
  

----
