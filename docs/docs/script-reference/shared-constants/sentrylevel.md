---
title: 'SentryLevel'
---
# `constants` SentryLevel <font size="4">(shared-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `SENTRY_LEVEL_DEBUG` | Represents debug event level. |
| `SENTRY_LEVEL_INFO` | Represents debug info level. |
| `SENTRY_LEVEL_WARNING` | Represents debug warning level. |
| `SENTRY_LEVEL_ERROR` | Represents debug error level. |
| `SENTRY_LEVEL_FATAL` | Represents debug fatal level. |
