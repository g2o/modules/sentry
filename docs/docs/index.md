# Home

This module provides a unofficial [**sentry.io**](https://sentry.io/) back-end for squirrel programming language using [**sentry-native**](https://github.com/getsentry/sentry-native) SDK.

Sentry can be used to report the runtime errors, so that you can later try to fix them.  
The main pros of this approach is that you can host a test server, and fix the issues found by the players later,  
rather than scrolling through console output or server.log file.