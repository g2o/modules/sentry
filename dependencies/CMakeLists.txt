add_subdirectory(sqapi)

set(SENTRY_BACKEND "none")
add_subdirectory(sentry-native)

target_link_libraries(${PROJECT_NAME}
	PRIVATE
		sqapi
		sentry
)